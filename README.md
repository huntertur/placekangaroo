# placekangaroo

This program is licensed under the AGPLv3.

## Running from a container

```sh
docker build . -t placekangaroo
docker run -p 8000:8000 placekangaroo
```
