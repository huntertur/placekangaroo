use image::ImageFormat;
use image::imageops::FilterType;
use std::path::Path;
use std::path::PathBuf;

use rand::prelude::*;

/// Fetch a random file from the given path.
pub fn random_file(path: &Path) -> Result<PathBuf, String> {
    let count = path.read_dir().map_err(|e| e.to_string())?.count();

    if count == 0 {
        return Err(String::from("no images exist"));
    }

    let index = rand::thread_rng().gen_range(0..count);
    let item = path.read_dir().map_err(|e| e.to_string())?.nth(index);

    match item {
        Some(result) => {
            let entry = result.map_err(|e| e.to_string())?;
            Ok(entry.path())
        },
        None => Err(String::from("could not find a random file"))
    }
}

/// Resize and crop the requested image.
pub fn resize_image(path: &Path, width: u32, height: u32)
    -> Result<Vec<u8>, String> {
    let img = image::open(path).map_err(|e| e.to_string())?;
    let resized = img.resize_to_fill(width, height, FilterType::Nearest);
    let mut bytes = vec![];
    resized.write_to(&mut bytes, ImageFormat::Jpeg).map_err(|e| e.to_string())?;
    Ok(bytes)
}
