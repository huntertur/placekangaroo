pub mod cache;
pub mod error;
pub mod lib;

use rocket::{catch, catchers, get, launch, routes};
use std::ops::Deref;
use rocket_dyn_templates::Template;
use std::sync::Mutex;
use rocket::State;
use std::collections::HashMap;
use std::path::Path;
use rocket::fs::{FileServer, relative};
use rocket::http::ContentType;
use rocket::http::Status;
use cache::{CacheMap, CropCache};

use error::to_500;

#[catch(404)]
fn catch_404() -> Template {
    let context: HashMap<(), ()> = HashMap::new();
    Template::render("404", context)
}

#[get("/cache")]
fn cache_info(state: &State<CropCache>) -> Result<Template, (Status, String)> {
    let mutex = state.cache.lock().map_err(to_500)?;
    let stats = cache::stats(mutex.deref());
    Ok(Template::render("cache", &stats))
}

#[get("/")]
fn index() -> Template {
    let context: HashMap<(), ()> = HashMap::new();
    Template::render("index", context)
}

#[get("/<width>/<height>")]
async fn random_cropped(state: &State<CropCache>, width: u32, height: u32)
    -> Result<(ContentType, Vec<u8>), (Status, String)> {
    if width == 0 || height == 0 {
        return Err((Status::BadRequest,
                    String::from("height and width must be positive")));
    }

    let path = Path::new(relative!("static")).join("images");
    let file = lib::random_file(&path).map_err(to_500)?;
    let mut cache = state.cache.lock().map_err(to_500)?;
    let os_string = file.as_os_str().into();
    let key = (width, height, os_string);
    let bytes = match cache.get(&key) {
        Some(value) => {
            eprintln!("Loading from cache: {:?}", key);
            value.clone()
        },
        None => {
            eprintln!("Adding to cache: {:?}", key);
            let new = lib::resize_image(&file, width, height).map_err(to_500)?;
            cache.insert(key, new.clone());
            new
        },
    };
    Ok((ContentType::JPEG, bytes))
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .configure(rocket::Config {
            address: std::net::Ipv4Addr::new(0, 0, 0, 0).into(),
            ..rocket::Config::default()
        })
        .manage(CropCache {
            cache: Mutex::new(CacheMap::new()),
        })
        .attach(Template::fairing())
        .mount("/", routes![cache_info, index, random_cropped])
        .mount("/", FileServer::from(relative!("static")))
        .register("/", catchers![catch_404])
}

#[cfg(test)]
mod tests {
    use super::*;
    use image::GenericImageView;
    use rocket::local::blocking::Client;

    #[test]
    fn index() {
        let client = Client::tracked(rocket()).unwrap();

        let response = client.get("/").dispatch();

        assert_eq!(Status::Ok, response.status());
    }

    #[test]
    fn random_cropped_valid() {
        let client = Client::tracked(rocket()).unwrap();

        // Use small values for test speed
        let response = client.get("/30/20").dispatch();

        assert_eq!(Status::Ok, response.status());
        assert_eq!(ContentType::JPEG, response.content_type().unwrap());
        let img = image::load_from_memory(&response.into_bytes().unwrap()[..])
                        .unwrap();
        assert_eq!(30, img.width());
        assert_eq!(20, img.height());
    }

    #[test]
    fn random_cropped_invalid() {
        let client = Client::tracked(rocket()).unwrap();

        let response = client.get("/0/0").dispatch();

        assert_eq!(Status::BadRequest, response.status());
        assert_ne!(ContentType::JPEG, response.content_type().unwrap());
    }

    #[test]
    fn cache_info_empty() {
        let client = Client::tracked(rocket()).unwrap();

        let response = client.get("/cache").dispatch();

        assert_eq!(Status::Ok, response.status());
        assert!(response.into_string().unwrap().contains('0'));
    }

    #[test]
    fn cache_info_one() {
        let client = Client::tracked(rocket()).unwrap();

        // Use small values for test speed
        client.get("/30/20").dispatch();
        let response = client.get("/cache").dispatch();

        assert_eq!(Status::Ok, response.status());
        assert!(response.into_string().unwrap().contains('1'));
    }

    #[test]
    fn catch_404() {
        let client = Client::tracked(rocket()).unwrap();

        let response = client.get("/cats").dispatch();

        assert_eq!(Status::NotFound, response.status());
        let body = response.into_string().unwrap();
        assert!(body.contains("Not Found"));
        assert!(body.contains("file an issue"));
    }
}
