use serde::Serialize;
use std::collections::HashMap;
use std::ffi::OsString;
use std::sync::Mutex;

/// The key is (width, height, image path).
pub type CacheMap = HashMap<(u32, u32, OsString), Vec<u8>>;

/// CacheMap wrapper for use as a Rocket State.
pub struct CropCache {
    pub cache: Mutex<CacheMap>,
}

/// Summary statistics of a CacheMap.
#[derive(Serialize)]
pub struct CacheStats {
    pub count: usize,
    pub total: usize,
}

/// Compute the summary statistics for a CacheMap.
pub fn stats(cache: &CacheMap) -> CacheStats {
    CacheStats {
        count: cache.len(),
        total: cache.iter()
                    .map(|(_, data)| { data.len() })
                    .reduce(|accumulator, size| { accumulator + size })
                    .unwrap_or(0),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn stats_empty_cache() {
        let cache = CacheMap::new();

        let stats = stats(&cache);

        assert_eq!(0, stats.count);
        assert_eq!(0, stats.total);
    }

    #[test]
    fn stats_one_item_cache() {
        let mut cache = CacheMap::new();
        cache.insert((1, 2, OsString::from("Foo")), vec![1, 2, 3]);

        let stats = stats(&cache);

        assert_eq!(1, stats.count);
        assert_eq!(3, stats.total);
    }

    #[test]
    fn stats_many_item_cache() {
        let mut cache = CacheMap::new();
        cache.insert((1, 2, OsString::from("Foo")), vec![1, 2, 3]);
        cache.insert((3, 4, OsString::from("Bar")), vec![1]);
        cache.insert((5, 6, OsString::from("Baz")), vec![1, 2, 3, 4, 5, 6]);

        let stats = stats(&cache);

        assert_eq!(3, stats.count);
        assert_eq!(10, stats.total);
    }
}
