use rocket::http::Status;

/// Use with map_err to turn a Result into a 500 response with a String.
pub fn to_500<T: ToString>(error: T) -> (Status, String) {
    (Status::InternalServerError, error.to_string())
}
