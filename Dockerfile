FROM rust:1.72.1-alpine3.18 AS builder
WORKDIR /usr/src/placekangaroo
COPY . .
RUN apk add --no-cache libc-dev
RUN cargo install --path .

FROM alpine:3.18
COPY static/ /usr/src/placekangaroo/static/
COPY templates/ /usr/src/placekangaroo/templates/
COPY --from=builder /usr/local/cargo/bin/placekangaroo /usr/local/bin/placekangaroo
WORKDIR /usr/src/placekangaroo/
CMD ["placekangaroo"]
